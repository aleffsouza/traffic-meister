import { Options } from "../../types";
import { mapStringArrToOptions, reduceUnique } from "../../utils";

export enum FilterType {
  Default = "default",
  Vehicle = "vehicle",
  Brand = "brand",
  Color = "color"
}

function colors(data: Array<any>): Options {
  const result: Array<string> = reduceUnique(data, "colors");
  const resultToOption = mapStringArrToOptions(result);
  return resultToOption;
}

function vehicles(data: Array<any>): Options {
  const result: Array<string> = reduceUnique(data, "type");
  const resultToOption = mapStringArrToOptions(result);
  return resultToOption;
}

function brands(data: Array<any>): Options {
  const arr = data.map(d => d.brand);
  const result = mapStringArrToOptions(arr);
  return result;
}

export default (data: Array<any>) => ({
  filterBy: (key: string, selectedValue: string): Array<any> =>
    data.filter((vehicle: any) => vehicle[key] === selectedValue),

  filterByColor: (selectedValue: string): Array<any> =>
    data.filter((vehicle: any) => vehicle.colors.indexOf(selectedValue) !== -1),

  filterOptions: (
    newSelectedOption: FilterType,
    oldFilteredOptions?: Array<Options>
  ): Array<Options> => {
    if (oldFilteredOptions) {
      let result: Array<Options>;
      switch (newSelectedOption) {
        case FilterType.Vehicle:
          result = [oldFilteredOptions[0], brands(data), colors(data)];
          break;
        case FilterType.Brand:
          result = [vehicles(data), oldFilteredOptions[1], colors(data)];
          break;
        case FilterType.Color:
          result = [vehicles(data), brands(data), oldFilteredOptions[2]];
          break;
        default:
          result = oldFilteredOptions;
      }
      return result;
    }

    const result: Array<Options> = [vehicles(data), brands(data), colors(data)];
    return result;
  }
});
