import Typography from "typography";

const defaultFonts = [
  "Segoe UI",
  "Helvetica Neue",
  "Helvetica",
  "Arial",
  "sans-serif"
];

export const typography = new Typography({
  bodyFontFamily: defaultFonts,
  baseFontSize: "18px",
  overrideStyles: () => ({
    "p, h1, h2, h3, h4, h5, h6": {
      fontFamily: defaultFonts.join(",")
    }
  }),
  blockMarginBottom: 0
});
