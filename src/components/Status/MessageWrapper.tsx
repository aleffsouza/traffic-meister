import styled from "styled-components";

const MessageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 50px 0;
  text-align: center;

  h4 {
    color: #707070;
  }

  img {
    margin-top: 30px;
  }
`;

export default MessageWrapper;
