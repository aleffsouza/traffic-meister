import * as React from "react";
import CardWrapper from "./CardWrapper";

interface CardProps {
  brand: string;
  img: string;
}

const Card = (props: CardProps) => (
  <CardWrapper {...props}>
    <div className="card-body-container">
      <h4>{props.brand}</h4>
    </div>
  </CardWrapper>
);

export default Card;