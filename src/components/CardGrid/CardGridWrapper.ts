import styled from "styled-components";

const CardGridWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
  grid-gap: 40px;
  padding: 50px 0;
`;

export default CardGridWrapper;