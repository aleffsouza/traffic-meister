import * as React from "react";
import Card from "../Card";
import { NoData } from "../Status";
import CardGridWrapper from "./CardGridWrapper";

function renderCards(data: Array<any>): React.ReactNode {
  return (
    <>
      {data.map((data, i) => (
        <Card key={i} {...data} />
      ))}
    </>
  );
}

interface CardGridProps {
  data: Array<any>;
}

const CardGrid = (props: CardGridProps) => {
  const { data } = props;
  if (data.length === 0) {
    return <NoData />;
  }
  
  return (
    <CardGridWrapper>
      {renderCards(data)}
    </CardGridWrapper>
  );
}

export default CardGrid;
