import * as React from "react";
import { TypographyStyle } from "react-typography";
import Header from "../../components/Header";
import { DataContext } from "../DataProvider";
import { ServiceError, Loading } from "../../components/Status";
import { typography } from "../../config";
import AppWrapper from "./AppWrapper";

const DropdownGroup = React.lazy(() =>
  import("../../components/DropdownGroup")
);
const CardGrid = React.lazy(() => import("../../components/CardGrid"));

export default class App extends React.Component<{}, {}> {
  private _renderAppContent(context: any) {
    let content = undefined;
    const { state, ...actions } = context;
    const { err, loading } = state;
    if (err) {
      content = <ServiceError />;
    } else if (loading) {
      content = <Loading />;
    } else {
      content = (
        <React.Suspense fallback={<Loading />}>
          <>
            <DropdownGroup
              loading={loading}
              filteredOptions={state.filteredOptions}
              handleChange={actions.selectOption}
            />
            <CardGrid data={state.filteredData} />
          </>
        </React.Suspense>
      );
    }
    return content;
  }

  render() {
    return (
      <AppWrapper>
        <TypographyStyle typography={typography} />
        <div className="page-container">
          <Header />
          <DataContext.Consumer>{this._renderAppContent}</DataContext.Consumer>
        </div>
      </AppWrapper>
    );
  }
}
