import * as React from "react";
import { Option } from "../../types";
import traficMeister from "../../services/TrafficMeister";
import FilterService, { FilterType } from "../../services/FilterService";

export const DataContext = React.createContext({});

interface AppState {
  loading?: boolean;
  data?: any;
  filteredData?: any;
  selectedFilterType: FilterType;
  err?: string;
  filteredOptions?: Array<Array<Option>>;
  selectedOption: Option;
}

export default class DataProvider extends React.Component<{}, AppState> {
  state: AppState = {
    loading: undefined,
    data: undefined,
    filteredData: undefined,
    err: undefined,
    selectedFilterType: FilterType.Default,
    filteredOptions: undefined,
    selectedOption: undefined
  };

  async componentDidMount() {
    this.setState({ loading: true });
    try {
      const data = await traficMeister.fetchData();
      const { filterOptions } = FilterService(data);
      this.setState({
        data,
        loading: false,
        filteredData: data,
        filteredOptions: filterOptions(this.state.selectedFilterType)
      });
    } catch (e) {
      this.setState({ err: e });
    }
  }

  private _handleSelectOption = (
    filterType: FilterType,
    newSelectedOption: Option
  ) => {
    const { data, filteredOptions } = this.state;
    const { filterBy, filterByColor } = FilterService(data);
    let newState: { selectedOption: Option } | AppState = {
      selectedOption: newSelectedOption
    };

    const newFilteredData = this._filter(
      filterType,
      newSelectedOption,
      filterBy,
      filterByColor
    );

    const oldFilteredOptions = filteredOptions;

    newState = {
      ...newState,
      selectedFilterType: filterType,
      filteredOptions: FilterService(newFilteredData).filterOptions(
        filterType,
        oldFilteredOptions
      ),
      filteredData: newFilteredData
    };

    this.setState(newState);
  };

  private _filter = (
    selectedFilter: FilterType,
    selectedOption: Option,
    filterBy: Function,
    filterByColor: Function
  ): Array<any> => {
    let result: Array<any>;
    switch (selectedFilter) {
      case FilterType.Vehicle:
        result = this._filterByVehicle(filterBy, selectedOption);
        break;
      case FilterType.Brand:
        result = this._filterByBrand(filterBy, selectedOption);
        break;
      case FilterType.Color:
        result = filterByColor(selectedOption.value);
        break;
      default:
        result = this.state.filteredData;
    }
    return result;
  };

  private _filterByVehicle = (filterBy: Function, selectedOption: Option) =>
    filterBy("type", selectedOption.value);
  private _filterByBrand = (filterBy: Function, selectedOption: Option) =>
    filterBy("brand", selectedOption.value);

  render() {
    const { filteredData, selectedOption } = this.state;
    const { filterBy, filterByColor } = FilterService(filteredData);
    return (
      <DataContext.Provider
        value={{
          state: this.state,
          filterByColor,
          filterByBrand: () => this._filterByBrand(filterBy, selectedOption),
          filterByVehicle: () =>
            this._filterByVehicle(filterBy, selectedOption),
          selectOption: this._handleSelectOption
        }}
      >
        {this.props.children}
      </DataContext.Provider>
    );
  }
}
