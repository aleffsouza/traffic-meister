import { describe } from "riteway";
import reduceUnique from "./reduceUnique";

describe("reduceUnique()", async assert => {
  const data = [
    {
      name: "John Doe",
      age: 26
    },
    {
      name: "Mary Doe",
      age: 45
    },
    {
      name: "John Doe",
      age: 20
    },
    {
      name: "John Snow",
      age: 26
    },
    {
      name: "Mary Jane",
      age: 26
    }
  ];

  {
    const actual = reduceUnique(data, "name");
    const expected = ["John Doe", "Mary Doe", "John Snow", "Mary Jane"];

    assert({
      given: "An array of user objects",
      should: "return all unique entries by name",
      actual,
      expected
    });
  }

  {
    const actual = reduceUnique(data, "age");
    const expected = [26, 45, 20];

    assert({
      given: "An array of user objects",
      should: "return all unique entries by age",
      actual,
      expected
    });
  }
});

describe("reduceUnique() should ignore entries without the given property", async assert => {
  const data = [
    {
      name: "John Doe",
      age: 26
    },
    {
      name: "Mary Doe"
    },
    {
      name: "John Doe",
      age: 20
    },
    {
      msg: "Hello World"
    }
  ];

  const actual = reduceUnique(data, "age");
  const expected = [26, 20];

  assert({
    given: "An array of objects",
    should: "ignore any entry without an 'age' property",
    actual,
    expected
  });
});

describe("reduceUnique() in an empty array", async assert => {
  const data = [];

  const actual = reduceUnique(data, "age");
  const expected = [];

  assert({
    given: "An empty array",
    should: "return an empty array",
    actual,
    expected
  });
});
