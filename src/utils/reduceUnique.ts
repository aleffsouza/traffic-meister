export default <T>(data: Array<any>, property: string): Array<T> =>
  data.reduce((previousEntry: any, currentEntry: any) => {
    const currentPropValue = currentEntry[property];
    if (currentPropValue) {
      const newEntry = new Set([...previousEntry, ...currentPropValue]);
      return Array.from(newEntry);
    }
    return previousEntry;
  }, []);
