import Option from "./Option";

type Options = Array<Option>;

export { Option, Options };
